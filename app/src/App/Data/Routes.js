import ViewPokedex from 'App/views/Pokedex'
import ViewCook from 'App/Views/Cook';

const notFound = () => (<div>Not Found</div>)

const RoutesList = [
  {
    label: "Pokédex",
    path: '/pokedex',
    key: 'pokedex',
    component: ViewPokedex,
    exact: true
  },
  {
    path: '/pokedex/:pokemon',
    key: 'pokedex_pokemon',
    component: ViewPokedex,
    exact: true
  },
  {
    label: "Cook",
    path: '/',
    key: 'home',
    component: ViewCook,
    exact: true
  },
  {
    path: '/404',
    key: 'notFound',
    component: notFound,
  }
]

export default RoutesList