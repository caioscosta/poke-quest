import * as THREE from 'three'

import FBXLoader from 'three-fbx-loader'
import GLTF2Loader from 'three-gltf2-loader'

import colors from 'Assets/styles/_colors.scss'

const OrbitControls = require('three-orbit-controls')(THREE)
GLTF2Loader(THREE)

let canvas = {}

const start = (callback) => {
    canvas.lastUpdate = Date.now()
    
    if (!canvas.frameId) {
        canvas.frameId = requestAnimationFrame(canvas.animate)
    }

    return callback && callback(canvas)
}	
const stop = () => {
    cancelAnimationFrame(canvas.frameId)
}
	
const animate = () => {
    const update = Date.now()
    const dt = update - canvas.lastUpdate
    canvas.lastUpdate = update

    if(canvas.scene) {
        canvas.scene.getObjectByName('monster').mixer.update(dt/1000)
    }

    canvas.renderScene()
    canvas.frameId = window.requestAnimationFrame(canvas.animate)

}	
const renderScene = () => {
    canvas.renderer ? canvas.renderer.render(canvas.scene, canvas.camera) : null
}


function threeCanvas ( params, callback) {
    if(!params.selected) return 

    const scene = new THREE.Scene()
    const camera = new THREE.OrthographicCamera(-5, 5, 4, -4, 0.01, 1000)
    const renderer = new THREE.WebGLRenderer({ antialias: true })
    const controls = new OrbitControls(camera, renderer.domElement)
    const loader = new THREE.GLTFLoader()
    const light = new THREE.DirectionalLight(0xffffff, 0.2)
    const material = new THREE.MeshLambertMaterial({ vertexColors: THREE.VertexColors })

    camera.position.set(-2.5, 2.8, 3.5)
    camera.rotation.set(-0.5, -0.55, -0.3)

    controls.minZoom = 0.75
    controls.maxZoom = 2
    controls.enablePan = false
    
    const filename = `PM${params.selected}.glb`
    let monster = null
    
    canvas = { start, stop, animate, renderScene }
    
    loader.load(`/assets/meshes/gltf/${filename}`, (model) => {
        
        monster = model.scene.children[0].children[0]
        monster.name = "monster"
        monster.mixer = new THREE.AnimationMixer(monster)
        monster.material = material
        monster.castShadow = true

        let form = new THREE.CylinderGeometry(3, 3, 0.5, 32);
        let material = new THREE.MeshLambertMaterial( {color: colors.grey} );

        let cyl = new THREE.Mesh(form, material)

        let helper = new THREE.BoxHelper(monster)
        let middle = new THREE.Vector3()
        helper.geometry.computeBoundingBox();
        
        middle.x = (helper.geometry.boundingBox.max.x + helper.geometry.boundingBox.min.x) / 2
        middle.y = (helper.geometry.boundingBox.max.y + helper.geometry.boundingBox.min.y) / 2
        middle.z = (helper.geometry.boundingBox.max.z + helper.geometry.boundingBox.min.z) / 2
        
        controls.target = middle
        controls.maxPolarAngle = Math.PI/2
        
        cyl.position.set(middle.x, -0.25, middle.z)
        cyl.rotation.set(-Math.PI, 0, 0)
        light.position.set(5, 5, 2.5)

        light.castShadow = true
        light.shadowDarkness = 1;
        monster.castShadow = true
        cyl.castShadow = true
        cyl.receiveShadow = true
        
        light.target = monster

        renderer.setClearColor(colors.darker)
        renderer.setSize(params.size, params.size * 0.8)
        renderer.shadowMap.enabled = true;

        let monsterAction = model.animations.filter(animation => (/M\d*_\w*/).test(animation.name))
        if(monsterAction.length) monster.mixer.clipAction(monsterAction[0]).play()
            
        let defaultAction = model.animations.filter(animation => (/Mmotion_idle_\w*/).test(animation.name))
        if(defaultAction.length) monster.mixer.clipAction(defaultAction[0]).play()

        scene.add(new THREE.AmbientLight(colors.white))
        scene.add(monster)
        scene.add(cyl)
        scene.add(light)
        scene.add(light.target)

        controls.update()
        
        canvas = { ...canvas, scene , camera , renderer , material , monster, filename }
        canvas.start(callback)

    }, (xhr)=>{
        //loading routine
    }, (err) => {
        console.error("Error: ", err)
    })


}

module.exports = threeCanvas