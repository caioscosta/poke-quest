import React, { Component } from 'react';
import { TransitionGroup, CSSTransition } from "react-transition-group";
import {
  Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import createBrowserHistory from 'history/createBrowserHistory'

import RoutesList from "App/data/Routes"
import Navbar from 'App/Components/Navbar'

const browserHistory = createBrowserHistory();
const styles = {};

class AppRouter extends Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route render={({ location }) => (
                    <div>
                        <TransitionGroup>
                            <CSSTransition key={location.key} classNames="main page" timeout={1000}>
                                <Switch location={location}>
                                    {RoutesList.map((route, index) => (
                                        <Route {...route} model={index} />
                                    ))}
                                    {/* <Route component={this.noFound} /> */}
                                </Switch>
                            </CSSTransition>
                        </TransitionGroup>
                        <Navbar route={location}/>
                    </div>
                )}/>
            </Router>
        );
    }
}


export default AppRouter;
