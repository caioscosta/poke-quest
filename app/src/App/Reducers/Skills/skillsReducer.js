const SKILLS_INITIAL_STATE = { list: [] }

export default (state = SKILLS_INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SET_DATA_SKILLS':
            let { data } = action
            
            if(Array.isArray(data)) {
                state.list = data
            } 

            return state
        default:
            return state
    }
}