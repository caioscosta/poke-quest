import DATA from 'App/Data/appData'
import getData from 'App/Services/getData';

export const selectMonster = (id) => {
    return dispatch => {
        getData({
            method: 'post',
            url: DATA.API.POKEMON,
            data: {
                option: 'F',
                dexID: id
            }
        }, list => {
            console.log(id, list)
            let pokemon = list[0] || null;
            dispatch({ type: 'SELECT_MONSTER', id, pokemon })
        })
    }
}

export const toogleViewType = (model) => {
    return dispatch => {
        dispatch({ type: 'TOGGLE_VIEW', model })
    }
}

export const pokedexData = data => {
	return dispatch => {
        dispatch({ type: 'SET_DATA_DEX', data })
    }
}
