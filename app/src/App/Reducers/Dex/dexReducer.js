const DEX_INITIAL_STATE = { selected: 1 , view_model: false, list: [], pokemon: null}

export default (state = DEX_INITIAL_STATE, action) => {
    switch(action.type) {
        
        case 'SET_DATA_DEX':
            let { data } = action
            
            if(Array.isArray(data)) {
                state.list = data
            } 

            return {...state}

        case 'SELECT_MONSTER':
            let { id, pokemon } = action
            if(id) {
                state.pokemon = pokemon
                state.selected = id
            }
            return {...state}

        case 'TOGGLE_VIEW':

            let {view_model} = state

            if(action.model === true || action.model === false) {
                state.view_model = action.model
            } else {
                state.view_model = !view_model
            }

            return {...state}
            
        default:
            return state
    }
}