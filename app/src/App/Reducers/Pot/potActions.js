export const selectIngredient = slug => {
    return dispatch => {
        dispatch({ type: 'SELECT_INGREDIENT', slug })
    }
}

export const addIngredient = (position, dbclick) => {
	return dispatch => {
		dispatch({ type: 'ADD_INGREDIENT', position, dbclick })
    }
}

export const removeIngredient = position => {
	return dispatch => {
		dispatch({ type: 'REMOVE_INGREDIENT', position })
	}
}

export const ingredientsData = data => {
	return dispatch => {
        dispatch({ type: 'SET_DATA_INGREDIENTS', data })
    }
}

export const setPotType = active => {
	return dispatch => {
        dispatch({ type: 'SET_POT_TYPE', active })
    }
}

export const potsData = data => {
	return dispatch => {
        dispatch({ type: 'SET_DATA_POTS', data })
    }
}
