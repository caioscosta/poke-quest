import React, { Component } from 'react';
import Cabinet from 'App/Components/Cabinet'
import Pantry from 'App/Components/Pantry'
import Main from 'App/Components/Main/'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

class ViewCook extends Component {
    render() {
        return <Main key={this.props.key} content={[Pantry, Cabinet]}/>
    }
}

export default DragDropContext(HTML5Backend)(ViewCook);
