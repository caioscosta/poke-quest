import React, { Component } from 'react'
import Router from 'App/Modules/AppRouter'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { pokedexData } from 'App/Reducers/Dex/dexActions';
import { skillsData } from 'App/Reducers/Skills/skillsActions';
import { ingredientsData, potsData } from 'App/Reducers/Pot/potActions';

import DATA from 'App/Data/appData'
import getData from 'App/Services/getData'
class App extends Component {

    componentWillMount() {
        getData({
            method: 'get',
            url: DATA.API.SKILLS
        }, list => this.props.skillsData(list))

        getData({
            method: 'get',
            url: DATA.API.INGREDIENTS
        }, list => this.props.ingredientsData(list))

        getData({
            method: 'post',
            url: DATA.API.POKEMON,
            data: {
                dropable: false
            }
        }, list => this.props.pokedexData(list))

        getData({
            method: 'get',
            url: DATA.API.POTS,
        }, list => this.props.potsData(list))
    }
    
    render() {
        return <Router />;
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ skillsData, ingredientsData, pokedexData, potsData }, dispatch)
export default connect(StateToProps, DispatchToProps)(App)
