import React, { Component } from 'react';
import { connect } from 'react-redux';

import { toCaptalize } from 'App/Helpers/String';
import CustomScroll from 'react-custom-scroll';

class SkillsBlock extends Component {

    skillItem(skill) {
        let { attack , iconPath, name, wait, id, conditions, description, stones } = skill;
        console.log(skill)

        return (
                <li className="skill_item" key={id}>
                    <p className="skill_item-header" >{toCaptalize(name.en)}</p>
                    <div className="skill_item-wrapper">
                        <div className="skill_item-icon" style = {{backgroundImage: `url(/assets/images/skillicons/button_skill_${iconPath}.png)`}} alt={name.en} />
                        <div className="skill_item-data">
                            <span className="skill_data-description" >{description}</span>
                        </div>
                        <div className="skill_item-values">
                            <span className="skill_value value-attack" title={`Skill Damage: ${attack}`}>{attack}</span>
                            <span className="skill_value value-wait" title={`Skill Cooldown: ${wait}`}>{wait}</span>
                        </div>
                    </div>
                    <div className="skill_item-details">
                        <span className="skill_detail-stones" >{stones.map((stone, i) => <img src={`/assets/images/stones/skill_${stone}.png`} key={i}/>)}</span>
                        {conditions.length ? <div className="skill_detail-conditions">
                            {conditions.map((condition, i) => <img src={`/assets/images/status/status_${condition.iconPath}.png`} title={`Duration: ${condition.time} Seconds`} key={i}/>)}
                        </div> : null }
                    </div>
                </li>
        )
    }

    renderSkillList() {
        let {pokemon} = this.props.dex
        let skillsList = this.props.skills.list

        if(pokemon && pokemon.skills.length) {
            skillsList = pokemon.skills
        }

        return(<CustomScroll allowOuterScroll={true}><ul> {skillsList.map((skill, i) => this.skillItem(skill))} </ul></CustomScroll>);
    }

    render() {
        return (
            <div id="SkillsList" >
                {console.log(this.props)}
                <div className="skills_list-wrapper">
                    {this.renderSkillList()}
                </div>
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
export default connect(StateToProps)(SkillsBlock)
