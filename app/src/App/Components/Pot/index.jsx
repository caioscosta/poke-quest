import React, { Component } from 'react';
import PotSlot from 'App/Components/PotSlot';
import { connect } from 'react-redux';

const maxSlots = 5;

class Pot extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }
    }

    renderSlots() {
        let slots = []

        for (let s = 0; s < maxSlots; s++ ){
            let ing = this.props.pot.recipe[s] || null
            slots.push(<PotSlot slotID={s} key={s} ingredient={ing}/>)
        }

        return slots
    }

    render() {
        return (
            <div id="potContainer" className={this.props.pot.active + "-pot pot_container"}>
                <div className="pot_cover-shoulder_strap">
                    <div className="pot_cover-shoulder_strap-brights">
                        <div className="pot_bright"></div>
                    </div>
                </div>

                <div className="pot_cover">
                    <div className="pot_cover-brights">
                        <div className="pot_bright"></div>
                        <div className="pot_bright"></div>
                    </div>
                </div>

                <div className="pot_base">

                    <div className="pot_base-slots">
                        {this.renderSlots()}
                    </div>
                    
                    <div className="pot_base-border-brights">
                        <div className="pot_bright"></div>
                        <div className="pot_bright"></div>
                        <div className="pot_bright"></div>
                    </div>

                    <div className="pot_base-shoulder_strap left-strap"></div>
                    <div className="pot_base-shoulder_strap right-strap"></div>

                </div>

                <div className="pot_base-details">
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                </div>

                <div className="pot_base-brights">
                    <div className="pot_bright"></div>
                    <div className="pot_bright"></div>
                </div>
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
export default connect(StateToProps)(Pot)
