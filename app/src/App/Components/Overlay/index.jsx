import React, { Component } from 'react';

class Overlay extends Component {
    render() {
        return (
            <div className="overlay page-transition-overlay">
                <div className="overlay-slice"></div>
                <div className="overlay-slice"></div>
                <div className="overlay-slice"></div>
                <div className="overlay-slice"></div>
                <div className="overlay-slice"></div>                
            </div>
        );
    }
}

export default Overlay;
