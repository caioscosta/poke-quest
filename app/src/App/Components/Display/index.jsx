import React, { Component } from 'react';
import { connect } from 'react-redux';
import Canvas from 'App/Components/Canvas' 

class Display extends Component {
	constructor(props) {
		super(props)

		this.state = {
			size: 0,
			selected: this.props.dex.selected
		}
	}
	
	componentDidMount() {
		let display = this.display || document.getElementById('Display')
		const size = parseFloat(getComputedStyle(display).width)
		const { selected } = this.props.dex

		this.setState({
			size,
			selected
		})
	}
	
	componentWillReceiveProps(props) {
		let { selected } = props.dex
		let update = this.state.selected !== selected

		if(update) this.setState({ selected })
	}

	render() {
		
		return (
			<div id="Display" className='display' ref={display => this.display = display}>
				<Canvas size={this.state.size} selected={this.state.selected} hidden={!this.state.size || !this.state.selected}/>
				<div className="loading-thumb" />
			</div>
		)
	} 
}

const StateToProps = state => ({ ...state })
export default connect(StateToProps)(Display)
