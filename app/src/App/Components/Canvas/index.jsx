import React, { Component } from 'react';
import threeCanvas from 'App/Modules/threeCanvas' 
import cls from 'classnames'

class displayCanvas extends Component {
    constructor(props) {
        super(props)

        this.state = {
            canvas: null,
            size: this.props.size,
            selected: this.props.selected
        }

        this.renderCanvas = this.renderCanvas.bind(this)
    }

    componentWillUnmount() {
		this.state.canvas ? this.state.canvas.stop() : null
    }

    renderCanvas() {    
        let { selected, size } = this.state
        
        threeCanvas({ selected, size }, (canvas) => {
            if(this.canvas) {
                this.canvas.innerHTML ='';
                this.canvas.appendChild(canvas.renderer.domElement)
                this.setState({ canvas })
            }
        })
    }
    
    componentDidMount() {
        this.renderCanvas()
    }
    
	componentWillReceiveProps(props) {
        let { selected, size } = props
		let update = this.state.selected !== selected || this.state.size !== size

        if(update) this.setState({ selected, size}, this.renderCanvas)        
        
    }

    render() {
        return <div id="Canvas" className={cls({ 'loaded': !!this.state.canvas })} ref={canvas => this.canvas = canvas}/>;
    }
}

export default displayCanvas;
