import React, { Component } from 'react';

class Button extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }
        this.renderButton = this.renderButton.bind(this);
    }

    renderButton(){
        let {className, ...props} = this.props
        return (
            <button className={"btn_default " + className} {...props}>
                {this.props.children}
            </button>
        );
    }

    render() {
        return this.renderButton()
    }
}

export default Button;
