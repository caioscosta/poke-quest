import React, { Component } from 'react';
import { connect } from 'react-redux';
import CustomScroll from 'react-custom-scroll';

import {toCaptalize, rmDash} from 'App/Helpers/String';

import Pokethumb from 'App/Components/Thumb';



class Pokedex extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        return (
            <div id="pokedex">
                <CustomScroll allowOuterScroll={true} >
                    <div className="monster-container">
                        
                        {this.props.dex.list.map((item, key) => {
                            let props = {
                                dexid: item.dexID,
                                name: rmDash(toCaptalize(item.name)),
                                key
                            }
                        
                            return <Pokethumb {...props}/>
                        })}
                    </div>
                </CustomScroll>
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
export default connect(StateToProps)(Pokedex)

