import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import DATA from 'App/Data/appData'
import getData from 'App/Services/getData'
import Pot from 'App/Components/Pot'
import Button from 'App/Components/Button'

import { setPotType } from 'App/Reducers/Pot/potActions';
class Cabinet extends Component {
	constructor(props) {
		super(props)
		this.state = {
			potList: [],
			activePot: 'brass',
		}
	}

	componentWillMount(){
		getData({
			method: 'get',
			url: DATA.API.POTS
		}, potList => {
			this.setState({ potList })
			this.props.setPotType('brass')
		})
	}

	changePot(activePot){
		this.props.setPotType(activePot)
		console.log(this.props)
    }

	render() {
		return (
			<div id="cabinet">
				<Pot/>
				<div className="pot-controllers">
					{this.state.potList.map((pot, i) => {
						return(
							<Button key = {i} className = {pot.name.toLowerCase() + "-button change-button"} 
								onClick={this.changePot.bind(this, pot.name.toLowerCase())}/>
						)
					})}
				</div>
			</div>
		);
	}
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ setPotType }, dispatch)

export default connect(StateToProps, DispatchToProps)(Cabinet)
