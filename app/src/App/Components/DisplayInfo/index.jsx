import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toogleViewType } from 'App/Reducers/Dex/dexActions'

import Info from "App/Components/Info"
import SkillsBlock from "App/Components/SkillsBlock"
import Display from "App/Components/Display"
import ControlBar from 'App/Components/ControlBar';

class DisplayInfo extends Component {

    viewSkillList() {
        this.props.toogleViewType(false)
    }

    viewModelBox() {
        this.props.toogleViewType(true)
    }
    render() {
        return (
            <div id="DisplayInfo">
                <Info />
                <ControlBar className={this.props.dex.view_model ? "no-fill" : ""}>
                    <button className="btn_view view-model_box" disabled={this.props.dex.view_model} onClick={this.viewModelBox.bind(this)} />
                    <button className="btn_view view-skill_list" disabled={!this.props.dex.view_model} onClick={this.viewSkillList.bind(this)} />
                </ControlBar>
                {(this.props.dex.view_model ? <Display /> : <SkillsBlock selecte={this.props.dex.selected}/>)}
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ toogleViewType }, dispatch)
export default connect(StateToProps, DispatchToProps)(DisplayInfo)
