import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Counter from 'App/Components/Counter';
import Ingredient from 'App/Components/Ingredient';

import DATA from 'App/Data/appData'
import getData from 'App/Services/getData';
import {toCaptalize, rmDash} from 'App/Helpers/String';
import CustomScroll from 'react-custom-scroll';

import { addIngredient, selectIngredient } from 'App/Reducers/Pot/potActions';

class Pantry extends Component {

	constructor(props) {
        super(props);

        this.state = {
			selected: null,
            ingredients: []
		}            
		
        this.selectIngredient = this.selectIngredient.bind(this);
    }

    componentWillMount(){
        getData({
            method: 'get',
            url: DATA.API.INGREDIENTS,
        }, ingredients => {
            this.setState({ ingredients })
        })
	}

	selectIngredient(slug){
		if(slug === this.props.pot.selected) {
			this.props.addIngredient(0, true)
		} else {
			this.props.selectIngredient(slug)
		}
    }
	
	render() {
		return (
			<div>
				<div id="Pantry">
					<div className="ingredient-container">
						{this.state.ingredients.map((item, key) => {
							let props = {
								key,
								img: item.img,
								slug: item.slug,
								name: rmDash(toCaptalize(item.name)),
								onClick: () => this.selectIngredient(item.slug)
							}
							return <Ingredient {...props} />
						})}
					</div>
				</div>			
				<Counter/>
			</div>
		);
	}
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ selectIngredient, addIngredient }, dispatch)

export default connect(StateToProps, DispatchToProps)(Pantry)