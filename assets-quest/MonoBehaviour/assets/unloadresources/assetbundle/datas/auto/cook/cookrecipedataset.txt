PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = -8506435790268521391
string m_Name = "CookRecipeDataSet"
CookRecipeData m_datas
	Array Array
	int size = 18
		[0]
		CookRecipeData data
			int m_no = 0
			int m_cookRank = 1
			int m_pokeType = 0
			int m_pokeTypeValue = 0
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 0
					[1]
					int data = 0
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 0
					[1]
					int data = 0
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 0
					[1]
					int data = 0
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_00"
			string m_modelPath = "BC_cookrecipe_00"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[1]
		CookRecipeData data
			int m_no = 1
			int m_cookRank = 4
			int m_pokeType = 1
			int m_pokeTypeValue = 1
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 0
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 0
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_01"
			string m_modelPath = "BC_cookrecipe_01"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[2]
		CookRecipeData data
			int m_no = 2
			int m_cookRank = 4
			int m_pokeType = 1
			int m_pokeTypeValue = 2
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 0
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 2
					[1]
					int data = 0
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_02"
			string m_modelPath = "BC_cookrecipe_02"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[3]
		CookRecipeData data
			int m_no = 3
			int m_cookRank = 4
			int m_pokeType = 1
			int m_pokeTypeValue = 3
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 0
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 0
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_03"
			string m_modelPath = "BC_cookrecipe_03"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[4]
		CookRecipeData data
			int m_no = 4
			int m_cookRank = 4
			int m_pokeType = 1
			int m_pokeTypeValue = 4
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 0
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_04"
			string m_modelPath = "BC_cookrecipe_04"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[5]
		CookRecipeData data
			int m_no = 5
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 10
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 2
					[1]
					int data = 1
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 2
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 3
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_05"
			string m_modelPath = "BC_cookrecipe_05"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[6]
		CookRecipeData data
			int m_no = 6
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 0
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 1
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 4
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_06"
			string m_modelPath = "BC_cookrecipe_06"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[7]
		CookRecipeData data
			int m_no = 7
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 3
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 1
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 3
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_07"
			string m_modelPath = "BC_cookrecipe_07"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[8]
		CookRecipeData data
			int m_no = 8
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 4
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 1
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 2
					[1]
					int data = 3
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_08"
			string m_modelPath = "BC_cookrecipe_08"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[9]
		CookRecipeData data
			int m_no = 9
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 11
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 2
					[1]
					int data = 1
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 2
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_09"
			string m_modelPath = "BC_cookrecipe_09"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[10]
		CookRecipeData data
			int m_no = 10
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 6
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 3
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 3
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 4
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_10"
			string m_modelPath = "BC_cookrecipe_10"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[11]
		CookRecipeData data
			int m_no = 11
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 13
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_11"
			string m_modelPath = "BC_cookrecipe_11"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[12]
		CookRecipeData data
			int m_no = 12
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 5
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 2
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 2
					[1]
					int data = 4
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_12"
			string m_modelPath = "BC_cookrecipe_12"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[13]
		CookRecipeData data
			int m_no = 13
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 2
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 3
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 2
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_13"
			string m_modelPath = "BC_cookrecipe_13"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[14]
		CookRecipeData data
			int m_no = 14
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 9
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 1
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 1
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_14"
			string m_modelPath = "BC_cookrecipe_14"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[15]
		CookRecipeData data
			int m_no = 15
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 12
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 2
					[1]
					int data = 1
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 1
					[1]
					int data = 3
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 3
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_15"
			string m_modelPath = "BC_cookrecipe_15"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[16]
		CookRecipeData data
			int m_no = 16
			int m_cookRank = 3
			int m_pokeType = 2
			int m_pokeTypeValue = 1
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 3
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 1
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 3
					[1]
					int data = 2
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_16"
			string m_modelPath = "BC_cookrecipe_16"
			UInt8 m_unlock = 0
			float m_cookTime = 2
		[17]
		CookRecipeData data
			int m_no = 17
			int m_cookRank = 2
			int m_pokeType = 3
			int m_pokeTypeValue = 5
			vector m_itemType
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemTypeValue
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 0
			vector m_itemPoolNumMin
				Array Array
				int size = 2
					[0]
					int data = 4
					[1]
					int data = 0
			vector m_itemPoolNumMax
				Array Array
				int size = 2
					[0]
					int data = 5
					[1]
					int data = 5
			float m_itemPriceAverage = 0
			string m_iconPath = "cookrecipe_17"
			string m_modelPath = "BC_cookrecipe_17"
			UInt8 m_unlock = 0
			float m_cookTime = 3
