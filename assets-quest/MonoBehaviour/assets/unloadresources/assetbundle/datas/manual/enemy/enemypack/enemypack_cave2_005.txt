PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 1088006837878153380
string m_Name = "EnemyPack_cave2_005"
EnemySet m_normalPrimaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 758003574390323494
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 7009562098005399819
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1867732920890351472
						int m_lotteryRate = 100
EnemySet m_normalSecondaryEnemySets
	Array Array
	int size = 4
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 4
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -1688427619667034937
						int m_lotteryRate = 120
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1118056917112126524
						int m_lotteryRate = 120
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5815816237713006249
						int m_lotteryRate = 120
					[3]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -8039197666266583496
						int m_lotteryRate = 40
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1867732920890351472
						int m_lotteryRate = 100
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 9046913525081005722
						int m_lotteryRate = 100
		[3]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 2009271499117063444
						int m_lotteryRate = 100
EnemySet m_normalTertiaryEnemySets
	Array Array
	int size = 3
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -8859180859675234483
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 3
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5879144613255257545
						int m_lotteryRate = 70
					[1]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -5031379763157299285
						int m_lotteryRate = 10
					[2]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 1674095497594384572
						int m_lotteryRate = 220
		[2]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 2009271499117063444
						int m_lotteryRate = 100
EnemySet m_normalQuarternaryEnemySets
	Array Array
	int size = 2
		[0]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = 2009271499117063444
						int m_lotteryRate = 100
		[1]
		EnemySet data
			RegistData m_registDatas
				Array Array
				int size = 1
					[0]
					RegistData data
						PPtr<$EnemyData> m_enemyData
							int m_FileID = 0
							SInt64 m_PathID = -8859180859675234483
						int m_lotteryRate = 100
EnemySet m_bossPrimaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = -2352035830024603984
				int m_lotteryRate = 100
EnemySet m_bossSecondaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 0
EnemySet m_bossTertiaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
EnemySet m_bossQuarternaryEnemySet
	RegistData m_registDatas
		Array Array
		int size = 1
			[0]
			RegistData data
				PPtr<$EnemyData> m_enemyData
					int m_FileID = 0
					SInt64 m_PathID = 0
				int m_lotteryRate = 0
RegistData m_registDatas
	Array Array
	int size = 13
		[0]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 1
			int m_spawnMode = 0
		[1]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 0
			int m_spawnMode = 0
		[2]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 3
		[3]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 3
			int m_spawnMode = 3
		[4]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 2
			int m_spawnMode = 3
		[5]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 3
			int m_setIndex = 0
			int m_spawnMode = 3
		[6]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 0
			int m_setIndex = 2
			int m_spawnMode = 2
		[7]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 2
			int m_max = 2
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 1
			int m_spawnMode = 2
		[8]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 1
			int m_spawnMode = 2
		[9]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 1
			int m_setType = 0
			int m_setIndex = 0
			int m_spawnMode = 0
		[10]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 1
			int m_setIndex = 2
			int m_spawnMode = 0
		[11]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 2
			int m_setIndex = 0
			int m_spawnMode = 0
		[12]
		RegistData data
			PPtr<$EnemyData> m_enemyData
				int m_FileID = 0
				SInt64 m_PathID = 0
			int m_min = 1
			int m_max = 1
			UInt8 m_isFixPosition = 0
			UInt8 m_isBoss = 0
			int m_setType = 3
			int m_setIndex = 0
			int m_spawnMode = 0
