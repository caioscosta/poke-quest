PPtr<GameObject> m_GameObject
	int m_FileID = 0
	SInt64 m_PathID = 0
UInt8 m_Enabled = 1
PPtr<MonoScript> m_Script
	int m_FileID = 0
	SInt64 m_PathID = 6635995965916352521
string m_Name = "StageRewardDataSet"
StageRewardData m_datas
	Array Array
	int size = 56
		[0]
		StageRewardData data
			int m_id = 0
			int m_dungeonID = 0
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 3
		[1]
		StageRewardData data
			int m_id = 1
			int m_dungeonID = 0
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 3
		[2]
		StageRewardData data
			int m_id = 2
			int m_dungeonID = 0
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 5
		[3]
		StageRewardData data
			int m_id = 3
			int m_dungeonID = 0
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 5
					[2]
					int data = 2
		[4]
		StageRewardData data
			int m_id = 4
			int m_dungeonID = 1
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 10
		[5]
		StageRewardData data
			int m_id = 5
			int m_dungeonID = 1
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 5
		[6]
		StageRewardData data
			int m_id = 6
			int m_dungeonID = 1
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 3
		[7]
		StageRewardData data
			int m_id = 7
			int m_dungeonID = 1
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 3
		[8]
		StageRewardData data
			int m_id = 8
			int m_dungeonID = 1
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 5
					[2]
					int data = 6
		[9]
		StageRewardData data
			int m_id = 9
			int m_dungeonID = 2
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 5
		[10]
		StageRewardData data
			int m_id = 10
			int m_dungeonID = 2
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 3
		[11]
		StageRewardData data
			int m_id = 11
			int m_dungeonID = 2
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 10
					[1]
					int data = 5
					[2]
					int data = 3
		[12]
		StageRewardData data
			int m_id = 12
			int m_dungeonID = 2
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 10
					[1]
					int data = 5
					[2]
					int data = 3
		[13]
		StageRewardData data
			int m_id = 13
			int m_dungeonID = 2
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 5
					[2]
					int data = 6
		[14]
		StageRewardData data
			int m_id = 14
			int m_dungeonID = 3
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 15
		[15]
		StageRewardData data
			int m_id = 15
			int m_dungeonID = 3
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 15
					[2]
					int data = 15
		[16]
		StageRewardData data
			int m_id = 16
			int m_dungeonID = 3
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 20
					[2]
					int data = 20
		[17]
		StageRewardData data
			int m_id = 17
			int m_dungeonID = 3
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 10
		[18]
		StageRewardData data
			int m_id = 18
			int m_dungeonID = 3
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 10
					[1]
					int data = 10
					[2]
					int data = 2
		[19]
		StageRewardData data
			int m_id = 19
			int m_dungeonID = 3
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 5
					[2]
					int data = 10
		[20]
		StageRewardData data
			int m_id = 20
			int m_dungeonID = 4
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 15
		[21]
		StageRewardData data
			int m_id = 21
			int m_dungeonID = 4
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 15
					[2]
					int data = 10
		[22]
		StageRewardData data
			int m_id = 22
			int m_dungeonID = 4
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 20
					[2]
					int data = 10
		[23]
		StageRewardData data
			int m_id = 23
			int m_dungeonID = 4
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 10
		[24]
		StageRewardData data
			int m_id = 24
			int m_dungeonID = 4
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 15
					[1]
					int data = 15
					[2]
					int data = 5
		[25]
		StageRewardData data
			int m_id = 25
			int m_dungeonID = 4
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 5
					[2]
					int data = 10
		[26]
		StageRewardData data
			int m_id = 26
			int m_dungeonID = 5
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 5
					[2]
					int data = 15
		[27]
		StageRewardData data
			int m_id = 27
			int m_dungeonID = 5
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 10
		[28]
		StageRewardData data
			int m_id = 28
			int m_dungeonID = 5
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 10
					[1]
					int data = 10
					[2]
					int data = 5
		[29]
		StageRewardData data
			int m_id = 29
			int m_dungeonID = 5
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 15
					[2]
					int data = 15
		[30]
		StageRewardData data
			int m_id = 30
			int m_dungeonID = 5
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 15
					[1]
					int data = 15
					[2]
					int data = 2
		[31]
		StageRewardData data
			int m_id = 31
			int m_dungeonID = 5
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 5
					[2]
					int data = 10
		[32]
		StageRewardData data
			int m_id = 32
			int m_dungeonID = 6
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 25
		[33]
		StageRewardData data
			int m_id = 33
			int m_dungeonID = 6
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 25
					[2]
					int data = 25
		[34]
		StageRewardData data
			int m_id = 34
			int m_dungeonID = 6
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 20
					[2]
					int data = 5
		[35]
		StageRewardData data
			int m_id = 35
			int m_dungeonID = 6
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 30
					[1]
					int data = 30
					[2]
					int data = 30
		[36]
		StageRewardData data
			int m_id = 36
			int m_dungeonID = 6
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 25
					[1]
					int data = 25
					[2]
					int data = 10
		[37]
		StageRewardData data
			int m_id = 37
			int m_dungeonID = 6
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 30
					[2]
					int data = 30
		[38]
		StageRewardData data
			int m_id = 38
			int m_dungeonID = 7
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 25
		[39]
		StageRewardData data
			int m_id = 39
			int m_dungeonID = 7
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 25
					[2]
					int data = 20
		[40]
		StageRewardData data
			int m_id = 40
			int m_dungeonID = 7
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 20
					[2]
					int data = 20
		[41]
		StageRewardData data
			int m_id = 41
			int m_dungeonID = 7
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 20
					[2]
					int data = 5
		[42]
		StageRewardData data
			int m_id = 42
			int m_dungeonID = 7
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 10
					[1]
					int data = 10
					[2]
					int data = 10
		[43]
		StageRewardData data
			int m_id = 43
			int m_dungeonID = 7
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 30
					[2]
					int data = 30
		[44]
		StageRewardData data
			int m_id = 44
			int m_dungeonID = 8
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 20
		[45]
		StageRewardData data
			int m_id = 45
			int m_dungeonID = 8
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 30
					[2]
					int data = 30
		[46]
		StageRewardData data
			int m_id = 46
			int m_dungeonID = 8
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 20
					[2]
					int data = 5
		[47]
		StageRewardData data
			int m_id = 47
			int m_dungeonID = 8
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 30
					[1]
					int data = 30
					[2]
					int data = 20
		[48]
		StageRewardData data
			int m_id = 48
			int m_dungeonID = 8
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 10
					[2]
					int data = 10
		[49]
		StageRewardData data
			int m_id = 49
			int m_dungeonID = 8
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 30
					[2]
					int data = 30
		[50]
		StageRewardData data
			int m_id = 50
			int m_dungeonID = 9
			int m_stageIndex = 0
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 10
					[2]
					int data = 15
		[51]
		StageRewardData data
			int m_id = 51
			int m_dungeonID = 9
			int m_stageIndex = 1
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 5
					[1]
					int data = 20
					[2]
					int data = 20
		[52]
		StageRewardData data
			int m_id = 52
			int m_dungeonID = 9
			int m_stageIndex = 2
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 20
					[1]
					int data = 15
					[2]
					int data = 5
		[53]
		StageRewardData data
			int m_id = 53
			int m_dungeonID = 9
			int m_stageIndex = 3
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 30
					[1]
					int data = 30
					[2]
					int data = 30
		[54]
		StageRewardData data
			int m_id = 54
			int m_dungeonID = 9
			int m_stageIndex = 4
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 1
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 10
					[1]
					int data = 10
					[2]
					int data = 10
		[55]
		StageRewardData data
			int m_id = 55
			int m_dungeonID = 9
			int m_stageIndex = 5
			vector m_rewardType
				Array Array
				int size = 3
					[0]
					int data = 4
					[1]
					int data = 3
					[2]
					int data = 3
			vector m_rewardID
				Array Array
				int size = 3
					[0]
					int data = 0
					[1]
					int data = 1
					[2]
					int data = 1
			vector m_rewardNum
				Array Array
				int size = 3
					[0]
					int data = 3
					[1]
					int data = 30
					[2]
					int data = 30
