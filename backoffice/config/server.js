const async = require('async')

const bodyParser = require('body-parser')
const express = require('express')
const server = express()
const allowCors = require('./cors')

const database = require('./database')

const gamePokemonDataToJSON = require("../services/gamePokemonDataToJSON")
const gameSkillDataToJSON = require("../services/gameSkillDataToJSON")
const gameSkillDependDataToJSON = require("../services/gameSkillDependDataToJSON")
const gameConditionDataToJSON = require("../services/gameConditionDataToJSON")
const gameConditionTypeDataToJSON = require("../services/gameConditionTypeDataToJSON")

const port = 3003

const setDishesList = require("../api/setDishesList")
const setPokemonList = require("../api/setPokemonList")
const setSkillsList = require("../api/setSkillsList")
const setQualityList = require("../api/setQualityList")
const setPotsList = require("../api/setPotsList")
const setTypesList = require("../api/setTypesList")
const setIngredientsList = require("../api/setIngredientsList")

server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())
server.use(allowCors)

database.connect((err, client) =>{
    let db = client.db(database.name)

    let startDefinitions = cb => cb(null, db)

    async.waterfall([
        startDefinitions,
        gamePokemonDataToJSON,
        gameSkillDataToJSON,
        gameSkillDependDataToJSON,
        gameConditionDataToJSON,
        gameConditionTypeDataToJSON,
        setTypesList,
        setSkillsList,
        setPokemonList,
        setDishesList,
        setQualityList,
        setPotsList,
        setIngredientsList
    ], (err, result) => {
        if(err) console.log(err)

        client.close()
        server.listen(port, () => {
            console.log(`Backend is running on port ${port}.`)
        })
    })
})

module.exports = server
