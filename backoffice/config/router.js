const express = require('express')

const getPokemon = require("../controllers/getPokemon")
const getDishes = require("../controllers/getDishes")
const getPots = require("../controllers/getPots")
const getSkills = require("../controllers/getSkills")
const getIngredients = require("../controllers/getIngredients")


module.exports = function(server) {
    // API Routes
    const router = express.Router()
    
    router.post('/pokemon', getPokemon);
    router.get('/dishes', getDishes);
    router.get('/pots', getPots);
    router.get('/skills', getSkills);
    router.get('/ingredients', getIngredients);
    
    server.use('/api', router)
}