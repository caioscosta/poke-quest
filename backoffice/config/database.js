const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://localhost:27017'

const database = {
    name : 'pokequest_content',
    connect : callback => {
        return MongoClient.connect(url, { useNewUrlParser: true }, callback)
    }
}

module.exports = database