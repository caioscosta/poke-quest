const apiData = require('../apiData.json')
const fs = require('fs')
const async = require('async')
const jsonfile = require('jsonfile')

let skillListFile = {skillList: []}
let path = apiData.path.skillicons

fs.readdir(path, (err, files) => {
	let n = 0
	let regex = /button_skill_([a-z].*)_([a-z].*).png$/

	async.map(files, (skill, cb) => {
		const skillObj = {
			icon: skill.replace('.png', ''),
			jp_name: skill.replace(regex, (str, type, name) => name),
			jp_type: skill.replace(regex, (str, type, name) => type),
			en_name: "",
			id: n
		}
		skillListFile.skillList.push(skillObj) 
		n++
		cb(null)
	}, error => {
		if(error) console.log(error)
		jsonfile.writeFile(`${apiData.path.data}/json/skillNames.json`, skillListFile, {spaces:4, EOL:"\r\n"}, error => error ? console.log(error) : null)
	})
})

