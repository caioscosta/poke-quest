// '\t\t[0]' => /\n\t.{1}\[(\d.*)\]\n/gm => `\t},\n\t{\n`
// 'int m_FileID = 0' => (\w.*\s)?(m_)?(\w.*)\s=\s(.*)\n => ``

const apiData = require('../apiData.json')
const fs = require('fs')
const jsonfile = require('jsonfile')

let path = apiData.path.game
let file_name = 'pokemondataset'

const gamePokemonDataToJSON = (db, cb) => {
    // console.log("run: gamePokemonDataToJSON")
    let target = `${apiData.path.data}/json/converted/${file_name}.json`

    fs.stat(target, (err, stat) => {
        if (err && err.code == 'ENOENT') {
            let sizes = /\t.*size = \d.*\n/gm
            let types = /(int\s|UInt8\s|SInt64\s|string\s|Array\s?|UInt16\s|vector\s|PPtr<MonoScript>\s|PPtr<GameObject>\s)/gm
            let props = /(monsterNo|hpBasis|attackBasis|hpGrow|attackGrow|type1|type2|visitWeightDefault|visitWeight|cookTableID|cookAllWeight|cookColorWeight|cookTypeWeight|cookLegendWeight|color|Rate|isLayer|meleePercent|growType|slotTypeWeightHp|slotTypeWeightAttack|slotTypeWeightMulti)\s=\s(\w.*)/gm
            let objs = /(potentialPercents|skillIDs)/gm
            let positions = /\t.*\[\d.*\]\n/gm
            let pkmDataList = /(\t.*PokemonData data)/gm
            let arrData = /(\t.*)data = (\w*)\n/gm
            let closeArrData = /(\t{1}"\d*"),(\s*"\w*":)/gm
            let toParseJson = /,(\s*{)/gm
            let endJson = /("\d.*")(,\n*$)/g
            
            fs.readFile(`${path}/${file_name}.txt`, 'utf8', (err, data) => {
                if (err) throw err;
                
                data = data.substring(data.indexOf('m_datas') + 'm_datas'.length);
                data = data.replace(/m_/gm, '');
                data = data.replace(/^/, "[\n")
                
                data = data.replace(sizes, '');
                data = data.replace(types, '');
                data = data.replace(positions, '');
                data = data.replace(props, `"$1":"$2",`);
                data = data.replace(objs, `"$1": [`);
                data = data.replace(pkmDataList, `\t\t{`)
                data = data.replace(arrData, `$1"$2",\n`)
                data = data.replace(closeArrData, `$1\n\t\t\t],\n$2`)
                data = data.replace(toParseJson, `]\n\t\t},$1`)
                data = data.replace(endJson, `$1]}]`)
                
                // console.log(data);
                
                let result = JSON.parse(data)

                jsonfile.writeFile(target, result, {spaces:4, EOL:"\r\n"}, error => {
                    if(error) console.log(error)
                    return cb && cb(null, db)
                })
            })
        } else {
            return cb && cb(null, db)
        }
    }); 
}

module.exports = gamePokemonDataToJSON