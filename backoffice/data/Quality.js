const Quality = [
    {
        name: "Normal",
        ingredients: "0",        
    },
    {
        name: "Good",
        ingredients: "1 or 2",        
    },
    {
        name: "Very Good",
        ingredients: "3 or 4",        
    },
    {
        name: "Special",
        ingredients: "5",        
    }
]

module.exports = Quality