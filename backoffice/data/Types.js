const Types = [
    {
        english: "Normal",
        japanese: "Normal",
        id: 0
    },
    {
        english: "Fighting",
        japanese: "Kakutou",
        id: 1
    },
    {
        english: "Flying",
        japanese: "Hikou",
        id: 2
    },
    {
        english: "Poison",
        japanese: "Doku",
        id: 3
    },
    {
        english: "Ground",
        japanese: "Jimen",
        id: 4
    },
    {
        english: "Rock",
        japanese: "Iwa",
        id: 5
    },
    {
        english: "Bug",
        japanese: "Mushi",
        id: 6
    },
    {
        english: "Ghost",
        japanese: "Ghost",
        id: 7
    },    
    {
        english: "Steel",
        japanese: "Hagane",
        id: 8
    },
    {
        english: "Fire",
        japanese: "Honoo",
        id: 9
    },
    {
        english: "Water",
        japanese: "Mizu",
        id: 10
    },
    {
        english: "Grass",
        japanese: "Kusa",
        id: 11
    },
    {
        english: "Electric",
        japanese: "Denki",
        id: 12
    },
    {
        english: "Psychic",
        japanese: "Esper",
        id: 13
    },
    {
        english: "Ice",
        japanese: "Koori",
        id: 14
    },
    {
        english: "Dragon",
        japanese: "Dragon",
        id: 15
    },
    {
        english: "Dark",
        japanese: "Aku",
        id: 16
    },
    {
        english: "Fairy",
        japanese: "Fairy",
        id: 17
    }
]

module.exports = Types