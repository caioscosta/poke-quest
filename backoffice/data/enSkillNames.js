const enSkillNames =
	[
		{ 
			"icon":"button_skill_aku_huiuti",
			"jp_name":"huiuti",
			"en_name":"sucker punch",
			"type":"dark"
		},
		{
			"icon":"button_skill_aku_kamikudaku",
			"jp_name":"kamikudaku",
			"en_name":"crunch",
			"type":"dark"
		},
		{
			"icon":"button_skill_aku_odateru",
			"jp_name":"odateru",
			"en_name":"flatter",
			"type":"dark"
		},
		{
			"icon":"button_skill_aku_tujigiri",
			"jp_name":"tujigiri",
			"en_name":"night slash",
			"type":"dark"
		},
		{
			"icon":"button_skill_aku_tyouhatu",
			"jp_name":"tyouhatu",
			"en_name":"taunt",
			"type":"dark"
		},
		{
			"icon":"button_skill_aku_warudakumi",
			"jp_name":"warudakumi",
			"en_name":"nasty plot",
			"type":"dark"
		},
		{
			"icon":"button_skill_denki_borutekkaa",
			"jp_name":"borutekkaa",
			"en_name":"volt tackle",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_denkisyokku",
			"jp_name":"denkisyokku",
			"en_name":"thunderbolt",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_erekihuiirudo",
			"jp_name":"erekihuiirudo",
			"en_name":"electric terrain",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_erekinetto",
			"jp_name":"erekinetto",
			"en_name":"electroweb",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_kaminari",
			"jp_name":"kaminari",
			"en_name":"thunder",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_kaminaripanti",
			"jp_name":"kaminaripanti",
			"en_name":"thunder punch",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_supaaku",
			"jp_name":"supaaku",
			"en_name":"spark",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_zyuuden",
			"jp_name":"zyuuden",
			"en_name":"charge",
			"type":"electric"
		},
		{
			"icon":"button_skill_denki_zyuumanboruto",
			"jp_name":"zyuumanboruto",
			"en_name":"thunderbolt",
			"type":"electric"
		},
		{
			"icon":"button_skill_doku_benomutorappu",
			"jp_name":"benomutorappu",
			"en_name":"venom drench",
			"type":"poison"
		},
		{
			"icon":"button_skill_doku_dokubari",
			"jp_name":"dokubari",
			"en_name":"poison sting",
			"type":"poison"
		},
		{
			"icon":"button_skill_doku_dokudoku",
			"jp_name":"dokudoku",
			"en_name":"toxic",
			"type":"poison"
		},
		{
			"icon":"button_skill_doku_dokunokona",
			"jp_name":"dokunokona",
			"en_name":"poison powder",
			"type":"poison"
		},
		{
			"icon":"button_skill_doku_hedorobakudan",
			"jp_name":"hedorobakudan",
			"en_name":"sludge bomb",
			"type":"poison"
		},
		{
			"icon":"button_skill_doku_sumoggu",
			"jp_name":"sumoggu",
			"en_name":"smog",
			"type":"poison"
		},
		{
			"icon":"button_skill_doku_tokeru",
			"jp_name":"tokeru",
			"en_name":"acid armor",
			"type":"poison"
		},
		{
			"icon":"button_skill_dragon_doragondaibu",
			"jp_name":"doragondaibu",
			"en_name":"dragon rush",
			"type":"dragon"
		},
		{
			"icon":"button_skill_dragon_doragonkurou",
			"jp_name":"doragonkurou",
			"en_name":"dragon claw",
			"type":"dragon"
		},
		{
			"icon":"button_skill_dragon_gekirin",
			"jp_name":"gekirin",
			"en_name":"outrage",
			"type":"dragon"
		},
		{
			"icon":"button_skill_dragon_ryuunohadou",
			"jp_name":"ryuunohadou",
			"en_name":"dragon pulse",
			"type":"dragon"
		},
		{
			"icon":"button_skill_dragon_ryuunomai",
			"jp_name":"ryuunomai",
			"en_name":"dragon dance",
			"type":"dragon"
		},
		{
			"icon":"button_skill_dragon_ryuuseigun",
			"jp_name":"ryuuseigun",
			"en_name":"draco meteor",
			"type":"dragon"
		},
		{
			"icon":"button_skill_dragon_tatumaki",
			"jp_name":"tatumaki",
			"en_name":"twister",
			"type":"dragon"
		},
		{
			"icon":"button_skill_esper_bariaa",
			"jp_name":"bariaa",
			"en_name":"barrier",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_dowasure",
			"jp_name":"dowasure",
			"en_name":"amnesia",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_hikarinokabe",
			"jp_name":"hikarinokabe",
			"en_name":"light screen",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_kousokuidou",
			"jp_name":"kousokuidou",
			"en_name":"agility",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_saikekouseni",
			"jp_name":"saikekouseni",
			"en_name":"psybeam",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_saikobureiku",
			"jp_name":"saikobureiku",
			"en_name":"psystrike",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_saikokattaa",
			"jp_name":"saikokattaa",
			"en_name":"psycho cut",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_saikokinesisu",
			"jp_name":"saikokinesisu",
			"en_name":"psychic",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_saiminzyutu",
			"jp_name":"saiminzyutu",
			"en_name":"hypnosis",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_sinennozutuki",
			"jp_name":"sinennozutuki",
			"en_name":"zen headbutt",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_terepooto",
			"jp_name":"terepooto",
			"en_name":"teleport",
			"type":"psychic"
		},
		{
			"icon":"button_skill_esper_yoganopoozu",
			"jp_name":"yoganopoozu",
			"en_name":"meditate",
			"type":"psychic"
		},
		{
			"icon":"button_skill_fairy_amaeru",
			"jp_name":"amaeru",
			"en_name":"charm",
			"type":"fairy"
		},
		{
			"icon":"button_skill_fairy_doreinkissu",
			"jp_name":"doreinkissu",
			"en_name":"draining kiss",
			"type":"fairy"
		},
		{
			"icon":"button_skill_fairy_majikarusyain",
			"jp_name":"majikarusyain",
			"en_name":"dazzling gleam",
			"type":"fairy"
		},
		{
			"icon":"button_skill_fairy_tensinokissu",
			"jp_name":"tensinokissu",
			"en_name":"sweet kiss",
			"type":"fairy"
		},
		{
			"icon":"button_skill_fairy_zyaretuku",
			"jp_name":"zyaretuku",
			"en_name":"play rough",
			"type":"fairy"
		},
		{
			"icon":"button_skill_ghost_ayasiihikari",
			"jp_name":"ayasiihikari",
			"en_name":"confuse ray",
			"type":"ghost"
		},
		{
			"icon":"button_skill_ghost_odorokasu",
			"jp_name":"odorokasu",
			"en_name":"astonish",
			"type":"ghost"
		},
		{
			"icon":"button_skill_ghost_sitadenameru",
			"jp_name":"sitadenameru",
			"en_name":"lick",
			"type":"ghost"
		},
		{
			"icon":"button_skill_ghost_syadoobooru",
			"jp_name":"syadoobooru",
			"en_name":"shadow ball",
			"type":"ghost"
		},
		{
			"icon":"button_skill_hagane_aianteeru",
			"jp_name":"aianteeru",
			"en_name":"iron tail",
			"type":"steel"
		},
		{
			"icon":"button_skill_hagane_haganenotubasa",
			"jp_name":"haganenotubasa",
			"en_name":"steel wing",
			"type":"steel"
		},
		{
			"icon":"button_skill_hagane_kinzokuon",
			"jp_name":"kinzokuon",
			"en_name":"metal sound",
			"type":"steel"
		},
		{
			"icon":"button_skill_hagane_metarukuroo",
			"jp_name":"metarukuroo",
			"en_name":"metal claw",
			"type":"steel"
		},
		{
			"icon":"button_skill_hagane_rasutaakanon",
			"jp_name":"rasutaakanon",
			"en_name":"flash cannon",
			"type":"steel"
		},
		{
			"icon":"button_skill_hagane_teppeki",
			"jp_name":"teppeki",
			"en_name":"iron defense",
			"type":"steel"
		},
		{
			"icon":"button_skill_hikou_bouhuu",
			"jp_name":"bouhuu",
			"en_name":"hurricane",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_dorirukutibasi",
			"jp_name":"dorirukutibasi",
			"en_name":"drill peck",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_goddobaado",
			"jp_name":"goddobaado",
			"en_name":"sky attack",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_haneyasume",
			"jp_name":"haneyasume",
			"en_name":"roost",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_kazeokosi",
			"jp_name":"kazeokosi",
			"en_name":"gust",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_oikaze",
			"jp_name":"oikaze",
			"en_name":"tailwind",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_sorawotobu",
			"jp_name":"sorawotobu",
			"en_name":"fly",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_tobihaneru",
			"jp_name":"tobihaneru",
			"en_name":"bounce",
			"type":"flying"
		},
		{
			"icon":"button_skill_hikou_tubamegaesi",
			"jp_name":"tubamegaesi",
			"en_name":"aerial ace",
			"type":"flying"
		},
		{
			"icon":"button_skill_honoo_daimonji",
			"jp_name":"daimonji",
			"en_name":"aerial ace",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_hinoko",
			"jp_name":"hinoko",
			"en_name":"ember",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_honoonopanti",
			"jp_name":"honoonopanti",
			"en_name":"fire punch",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_honoonouzu",
			"jp_name":"honoonouzu",
			"en_name":"fire spin",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_hunen",
			"jp_name":"hunen",
			"en_name":"lava plume",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_hureadoraibu",
			"jp_name":"hureadoraibu",
			"en_name":"flare blitz",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_kaenguruma",
			"jp_name":"kaenguruma",
			"en_name":"flame wheel",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_kaenhosya",
			"jp_name":"kaenhosya",
			"en_name":"flamethrower",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_neppuu",
			"jp_name":"neppuu",
			"en_name":"heat wave",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_nitorotyaazi",
			"jp_name":"nitorotyaazi",
			"en_name":"flame charge",
			"type":"fire"
		},
		{
			"icon":"button_skill_honoo_onibi",
			"jp_name":"onibi",
			"en_name":"will-o-wisp",
			"type":"fire"
		},
		{
			"icon":"button_skill_iwa_gansekihuuji",
			"jp_name":"gansekihuuji",
			"en_name":"rock tomb",
			"type":"rock"
		},
		{
			"icon":"button_skill_iwa_iwaotosi",
			"jp_name":"iwaotosi",
			"en_name":"rock throw",
			"type":"rock"
		},
		{
			"icon":"button_skill_iwa_korogaru",
			"jp_name":"korogaru",
			"en_name":"rollout",
			"type":"rock"
		},
		{
			"icon":"button_skill_iwa_rokkuburasuto",
			"jp_name":"rokkuburasuto",
			"en_name":"rock blast",
			"type":"rock"
		},
		{
			"icon":"button_skill_iwa_rokkukatto",
			"jp_name":"rokkukatto",
			"en_name":"rock polish",
			"type":"rock"
		},
		{
			"icon":"button_skill_iwa_sunaarasi",
			"jp_name":"sunaarasi",
			"en_name":"sandstorm",
			"type":"rock"
		},
		{
			"icon":"button_skill_iwa_suterusurokku",
			"jp_name":"suterusurokku",
			"en_name":"stealth rock",
			"type":"rock"
		},
		{
			"icon":"button_skill_jimen_anawohoru",
			"jp_name":"anawohoru",
			"en_name":"dig",
			"type":"ground"
		},
		{
			"icon":"button_skill_jimen_dorobakudan",
			"jp_name":"dorobakudan",
			"en_name":"mud bomb",
			"type":"ground"
		},
		{
			"icon":"button_skill_jimen_dorokake",
			"jp_name":"dorokake",
			"en_name":"mud-slap",
			"type":"ground"
		},
		{
			"icon":"button_skill_jimen_honebuumeran",
			"jp_name":"honebuumeran",
			"en_name":"bonemerang",
			"type":"ground"
		},
		{
			"icon":"button_skill_jimen_jisin",
			"jp_name":"jisin",
			"en_name":"earthquake",
			"type":"ground"
		},
		{
			"icon":"button_skill_jimen_makibisi",
			"jp_name":"makibisi",
			"en_name":"spikes",
			"type":"ground"
		},
		{
			"icon":"button_skill_jimen_sunaatume",
			"jp_name":"sunaatume",
			"en_name":"shore up",
			"type":"ground"
		},
		{
			"icon":"button_skill_kakutou_bakuretupanti",
			"jp_name":"bakuretupanti",
			"en_name":"dynamic punch",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_birudoappu",
			"jp_name":"birudoappu",
			"en_name":"bulk up",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_doreinpanti",
			"jp_name":"doreinpanti",
			"en_name":"drain punch",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_guroupanti",
			"jp_name":"guroupanti",
			"en_name":"power-up punch",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_infaito",
			"jp_name":"infaito",
			"en_name":"close combat",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_iwakudaki",
			"jp_name":"iwakudaki",
			"en_name":"rock smash",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_kurosutyoppu",
			"jp_name":"kurosutyoppu",
			"en_name":"cross chop",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_mawasigeri",
			"jp_name":"mawasigeri",
			"en_name":"rolling kick",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_tobihizageri",
			"jp_name":"tobihizageri",
			"en_name":"high jump kick",
			"type":"fighting"
		},
		{
			"icon":"button_skill_kakutou_zigokuguruma",
			"jp_name":"zigokuguruma",
			"en_name":"submission",
			"type":"fighting"
		},
		{
			"icon":"button_skill_koori_hubuki",
			"jp_name":"hubuki",
			"en_name":"blizzard",
			"type":"ice"
		},
		{
			"icon":"button_skill_koori_kogoerukaze",
			"jp_name":"kogoerukaze",
			"en_name":"icy wind",
			"type":"ice"
		},
		{
			"icon":"button_skill_koori_oororabeeru",
			"jp_name":"oororabeeru",
			"en_name":"aurora veil",
			"type":"ice"
		},
		{
			"icon":"button_skill_koori_reitoubiimu",
			"jp_name":"reitoubiimu",
			"en_name":"ice beam",
			"type":"ice"
		},
		{
			"icon":"button_skill_koori_reitoupanti",
			"jp_name":"reitoupanti",
			"en_name":"ice punch",
			"type":"ice"
		},
		{
			"icon":"button_skill_koori_turaraotosi",
			"jp_name":"turaraotosi",
			"en_name":"icicle crash",
			"type":"ice"
		},
		{
			"icon":"button_skill_kusa_hanabiranomai",
			"jp_name":"hanabiranomai",
			"en_name":"petal dance",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_happakattar",
			"jp_name":"happakattar",
			"en_name":"razor leaf",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_kinokonohousi",
			"jp_name":"kinokonohousi",
			"en_name":"spore",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_kougousei",
			"jp_name":"kougousei",
			"en_name":"synthesis",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_megadorein",
			"jp_name":"megadorein",
			"en_name":"mega drain",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_shibiregona",
			"jp_name":"shibiregona",
			"en_name":"stun spore",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_sooraabiimu",
			"jp_name":"sooraabiimu",
			"en_name":"solar beam",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_tanemasingan",
			"jp_name":"tanemasingan",
			"en_name":"bullet seed",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_turunomuti",
			"jp_name":"turunomuti",
			"en_name":"vine whip",
			"type":"grass"
		},
		{
			"icon":"button_skill_kusa_yadoriginotane",
			"jp_name":"yadoriginotane",
			"en_name":"leech seed",
			"type":"grass"
		},
		{
			"icon":"button_skill_mizu_akuaringu",
			"jp_name":"akuaringu",
			"en_name":"aqua ring",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_akuazyetto",
			"jp_name":"akuazyetto",
			"en_name":"aqua jet",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_awa",
			"jp_name":"awa",
			"en_name":"bubble",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_haidoroponpu",
			"jp_name":"haidoroponpu",
			"en_name":"hydro pump",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_karanikomoru",
			"jp_name":"karanikomoru",
			"en_name":"withdraw",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_naminori",
			"jp_name":"naminori",
			"en_name":"surf",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_takinobori",
			"jp_name":"takinobori",
			"en_name":"waterfall",
			"type":"water"
		},
		{
			"icon":"button_skill_mizu_uzusio",
			"jp_name":"uzusio",
			"en_name":"whirlpool",
			"type":"water"
		},
		{
			"icon":"button_skill_mushi_ginironokaze",
			"jp_name":"ginironokaze",
			"en_name":"silver wind",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_ikarinokona",
			"jp_name":"ikarinokona",
			"en_name":"rage powder",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_itowohaku",
			"jp_name":"itowohaku",
			"en_name":"string shot",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_kyuuketu",
			"jp_name":"kyuuketu",
			"en_name":"leech life",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_megahoon",
			"jp_name":"megahoon",
			"en_name":"megahorn",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_misairubari",
			"jp_name":"misairubari",
			"en_name":"pin missile",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_tobikakaru",
			"jp_name":"tobikakaru",
			"en_name":"lunge",
			"type":"bug"
		},
		{
			"icon":"button_skill_mushi_tonbogaeri",
			"jp_name":"tonbogaeri",
			"en_name":"u-turn",
			"type":"bug"
		},
		{
			"icon":"button_skill_normal_daibakuhatu",
			"jp_name":"daibakuhatu",
			"en_name":"explosion",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_gigainpakuto",
			"jp_name":"gigainpakuto",
			"en_name":"giga impact",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_hakaikousen",
			"jp_name":"hakaikousen",
			"en_name":"hyper beam",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_haneru",
			"jp_name":"haneru",
			"en_name":"splash",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_haradaiko",
			"jp_name":"haradaiko",
			"en_name":"belly drum",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_hensin",
			"jp_name":"hensin",
			"en_name":"transform",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_hikkaku",
			"jp_name":"hikkaku",
			"en_name":"scratch",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_hoeru",
			"jp_name":"hoeru",
			"en_name":"roar",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_hukitobasi",
			"jp_name":"hukitobasi",
			"en_name":"whirlwind",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_humituke",
			"jp_name":"humituke",
			"en_name":"stomp",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_hurassyu",
			"jp_name":"hurassyu",
			"en_name":"flash",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_huruitateru",
			"jp_name":"huruitateru",
			"en_name":"work up",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_jibaku",
			"jp_name":"jibaku",
			"en_name":"self-destruct",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_jitabata",
			"jp_name":"jitabata",
			"en_name":"flail",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_katakunaru",
			"jp_name":"katakunaru",
			"en_name":"harden",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_kiaidame",
			"jp_name":"kiaidame",
			"en_name":"focus energy",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_konoyubitomare",
			"jp_name":"konoyubitomare",
			"en_name":"follow me",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_megatonpanti",
			"jp_name":"megatonpanti",
			"en_name":"mega punch",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_midarehikkaki",
			"jp_name":"midarehikkaki",
			"en_name":"fury swipes",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_migawari",
			"jp_name":"migawari",
			"en_name":"substitute",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_nakigoe",
			"jp_name":"nakigoe",
			"en_name":"growl",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_nemuru",
			"jp_name":"nemuru",
			"en_name":"rest",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_niramitukeru",
			"jp_name":"niramitukeru",
			"en_name":"leer",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_renzokupanti",
			"jp_name":"renzokupanti",
			"en_name":"comet punch",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_sinsoku",
			"jp_name":"sinsoku",
			"en_name":"extreme speed",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_taiatari",
			"jp_name":"taiatari",
			"en_name":"tackle",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_tamagobakudan",
			"jp_name":"tamagobakudan",
			"en_name":"egg bomb",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_tamagoumi",
			"jp_name":"tamagoumi",
			"en_name":"soft-boiled",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_tatakitukeru",
			"jp_name":"tatakitukeru",
			"en_name":"slam",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_toraiatakku",
			"jp_name":"toraiatakku",
			"en_name":"tri attack",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_tossin",
			"jp_name":"tossin",
			"en_name":"take down",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_turuginomai",
			"jp_name":"turuginomai",
			"en_name":"swords dance",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_tyouonpa",
			"jp_name":"tyouonpa",
			"en_name":"supersonic",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_utau",
			"jp_name":"utau",
			"en_name":"sing",
			"type":"normal"
		},
		{
			"icon":"button_skill_normal_zikosaisei",
			"jp_name":"zikosaisei",
			"en_name":"recover",
			"type":"normal"
		}
	]

module.exports = enSkillNames