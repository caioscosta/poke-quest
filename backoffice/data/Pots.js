const Pots = [
    {
        name: "brass",
        requires: 3,
        min: 1,
        bonus: 0,
        iv: "0-10",
        quality: [
            {
                id: 0,
                turns: 2
            },
            {
                id: 1,
                turns: 4
            },
            {
                id: 2,
                turns: 5
            },
            {
                id: 3,
                turns: 6
            }
        ]
    },
    {
        name: "bronze",
        requires: 10,
        min: 20,
        bonus: 50,
        iv: "0-50",
        quality: [
            {
                id: 0,
                turns: 2
            },
            {
                id: 1,
                turns: 4
            },
            {
                id: 2,
                turns: 5
            },
            {
                id: 3,
                turns: 6
            }
        ]
    },
    {
        name: "silver",
        requires: 15,
        min: 40,
        bonus: 100,
        iv: "0-100",
        quality: [
            {
                id: 0,
                turns: 3
            },
            {
                id: 1,
                turns: 5
            },
            {
                id: 2,
                turns: 6
            },
            {
                id: 3,
                turns: 7
            }
        ]
    },
    {
        name: "gold",
        requires: 20,
        min: 70,
        bonus: 300,
        iv: "0-100",
        quality: [
            {
                id: 0,
                turns: 4
            },
            {
                id: 1,
                turns: 6
            },
            {
                id: 2,
                turns: 7
            },
            {
                id: 3,
                turns: 8
            }
        ]
    }
]

module.exports = Pots