const parseSearchURL = search => {
    const strObj = '{"' + decodeURI(search).replace(/\?/, '').replace(/&/g, '","').replace(/=/g,'":"') + '"}'
    try {
        return JSON.parse(strObj)
    } catch (err) {
        return null
    }
}

module.exports = parseSearchURL