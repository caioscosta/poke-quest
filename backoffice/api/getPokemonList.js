const async = require('async')
const getPokemonDishes = require('./getPokemonDishes')
const getPokemonSkills = require('./getPokemonSkills')
const database = require('../config/database');

const getPokemonList = (client, data, cb) => {
	let db = client.db(database.name)
    let pkmCollection = db.collection("pokemonList")
    let typesCollection = db.collection("typesList")

    let { option, name, dexID } = data
    let pokes = []
    let query = {}

    if(name && dexID) {
        pokes = pokes.concat(name, dexID)
    } else if (name) {
        pokes = pokes.concat(name)
    } else if (dexID) {
        pokes = pokes.concat(dexID)
    }

    if(pokes.length) {
        query = { $or: [] }
        pokes.forEach(poke => {
            query.$or.push({ name: poke.toLocaleString().toLowerCase() })
            query.$or.push({ dexID: parseInt(poke) || 0 })
        })
    }

    typesCollection.find({}).toArray((err, typesList) => {
        if(err) console.log(err)

        pkmCollection.find(query).sort({ dexID : 1 }).toArray((err, docs) => {
            
            if(docs.length) {
                if(data.dropable) {
                    docs = docs.filter(item => Number(item.gameData.cookTableID) !== 0)
                }

                async.map(docs, (doc, callback) => {
                    let { gameData, ...obj} = doc
                    const { skillIDs, type1, type2, ...data } = gameData
                        obj.types = typesList.filter(type => type.id == type1 || type.id == type2).map(type => type.english)
                        
                        if(option) {
                            if(option === 'G') {
                                callback(null, { ...obj, gameData: data })

                            } else if(option === 'D') {
                                getPokemonDishes(client, { pokemonName: obj.name }, (dishes) => {
                                    callback(null, { ...obj, dishes })
                                })
                            } else if(option === 'S') {
                                getPokemonSkills(db, skillIDs, skills => {				
                                    callback(null, { ...obj, skills })
                                })

                            } else if (option === 'F' && pokes.length) {
                                getPokemonSkills(db, skillIDs, skills => {
                                    getPokemonDishes(client, { pokemonName: obj.name }, (dishes) => {
                                        callback(null, { ...obj, dishes, skills, gameData: data })
                                    })
                                })
                            } else {
                                callback(null, obj)
                            }
                        } else {
                            callback(null, obj)
                        }
                    }, (err, result) => {
                        
                        return !err && cb && cb(result)
                    })            
            } else {
                return cb('Unregistered pokemon')
            }
        })
    })
}

module.exports = getPokemonList
