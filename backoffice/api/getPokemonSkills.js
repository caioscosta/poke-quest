const async = require('async')
const database = require('../config/database');

const getPokemonSkills = (db, skills, cb) => {
	let collection = db.collection("skillsList")
	
    let query = {}
    if(skills && skills.length) query = { id: { $in : skills } }
    // console.log(query);

    collection.find(query).toArray((err, docs) => {
		if(err) console.log(err)
		
		async.map(docs, (skill, callback) => {
            
			callback(null, skill)
			
        }, (err, result) => {
            return cb && cb(result) || result || []
        })
    })
}
module.exports = getPokemonSkills