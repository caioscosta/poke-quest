
const qualityList = require('../data/Quality')

const dbPublishList = require('./dbPublishList')

const setQualityList = (db, cb) => {
    // console.log("run: setQualityList")
	let collection = db.collection("qualityList")

	let list = qualityList.map((obj, id) => {
		obj.id = id
		return obj
	})

	let params = {
		db, 
		list,
		collection, 
		prop: 'id',
		name: 'QUALITY LIST'
	}

	dbPublishList(params, cb)
}

module.exports = setQualityList