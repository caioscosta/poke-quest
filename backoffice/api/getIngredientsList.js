const database = require('../config/database');

const getIngredientsList = (client, cb) => {
	let db = client.db(database.name)
	let collection = db.collection("ingredientsList")
	let query = {}
	
	collection.find(query).toArray((err, docs) => {
		cb(docs)
	})
}
module.exports = getIngredientsList