
const dbPublishList = require('./dbPublishList')
const async = require('async');

const getTypesList = require('./getTypesList')

let enSkillNames = require('../data/enSkillNames.js')
let skillsListData = require('../data/json/skillsListData.json')
let skillsGameData = require('../data/json/converted/skilldataresourcesset.json')
let skillsDependData = require('../data/json/converted/skilldependdataset.json')
let conditionData = require('../data/json/converted/conditiondataset.json')
let conditionTypeData = require('../data/json/converted/conditiontypedataset.json')


const setSkillsList = (db, cb) => {
    // console.log("run: setSkillsList")
    let collection = db.collection("skillsList")
    
    const SP_skills = skillsGameData.filter(skill => skill.skillPath.match(/^SP_/))
    
    async.map(SP_skills, (skill, _cb) => {
        let {id, skillPath, iconPath, damagePercent, chargeSecond, isEnablePotential_Nway, isEnablePotential_Extend, isEnablePotential_Continue, isEnablePotential_Charge, isEnablePotential_BuffTimeUp, isEnablePotential_BuffShare } = skill
        id = id.toLocaleString()
        
        let skillType = skillPath.replace(/^SP_(\w*)_(\w*)$/, '$1')
        
        getTypesList(db, typesList => {
            async.filter(typesList, (type, __cb) => {
                let err = skillType !== type.japanese
                return __cb(null, !err)
            }, (err, type) => {
                let stones = []
                let description = ""
                let jp_name = iconPath.replace(/^(\w*)_(\w*)$/, '$2').toLowerCase();

                let currSkill = enSkillNames.filter(skill => skill.jp_name === jp_name)[0]
                let en_name = currSkill ? currSkill.en_name : '';

                if(en_name) {
                    let skillData = skillsListData.filter(skill => skill.name == en_name)[0];
                    description = skillData ? skillData.description : ""
                }

                let name = {
                    jp: jp_name,
                    en: en_name
                }

                let skillsDepend = skillsDependData.filter( o => o.id == id)[0]
                let conditions = skillsDepend.conditions || []

                conditions = conditions.map(c => {
                    condition = conditionData.filter( o => parseInt(o.id) === c)[0]
                    conditionType = conditionTypeData.filter(type => condition.type === type.id)[0]

                    return {
                        time: condition.time,
                        iconPath : conditionType.iconPath.replace(/UI_ss_[n|p]_(\w*)/, '$1')
                    }
                })

                if(parseInt(isEnablePotential_Nway)) stones.push("newline")
                if(parseInt(isEnablePotential_Extend)) stones.push("extend")
                if(parseInt(isEnablePotential_Continue)) stones.push("continue")
                if(parseInt(isEnablePotential_Charge)) stones.push("charge")
                if(parseInt(isEnablePotential_BuffTimeUp)) stones.push("bufftimeup")
                if(parseInt(isEnablePotential_BuffShare)) stones.push("buffshare")

                if(!type[0]){
                    console.log('MISSING:type => ', jp_name)
                }
                
                if(!currSkill) {
                    console.log('MISSING:en_name => ', jp_name)
                }

                if(!description) {
                    console.log('MISSING:description =>', jp_name, en_name ? `| ${en_name}` :'')
                }

                _cb(null, {
                    id,
                    name,
                    stones,
                    iconPath,
                    conditions,
                    description,
                    type: type[0].english,
                    attack: parseInt(damagePercent*100),
                    wait: chargeSecond
                })
            })
        })

    }, (err, list) => {
        if(err) console.log(err)
        
        let params = {
            db,
            list,
            collection, 
            prop: 'iconPath',
            name: 'Skills LIST'
        }
    
        dbPublishList(params, cb)
    })
}

module.exports = setSkillsList