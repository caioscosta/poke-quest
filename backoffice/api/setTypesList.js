
const typesList = require('../data/Types')

const dbPublishList = require('./dbPublishList')

const setTypesList = (db, cb) => {
    // console.log("run: setTypesList")
	let collection = db.collection("typesList")
	
	let params = {
		db,
		list: typesList,
		collection, 
		prop: 'id',
		name: 'TYPES LIST'
	}

	dbPublishList(params, cb)
}

module.exports = setTypesList