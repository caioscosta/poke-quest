const database = require('../config/database');
const getPokemonList = require("../api/getPokemonList")

const getPokemon = (req, res, next) => {
    
    database.connect((err, client) => {        
        getPokemonList(client, req.body, list => res.json(list))
    })
}

module.exports = getPokemon