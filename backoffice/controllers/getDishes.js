const database = require('../config/database');
const getPokemonDishes = require("../api/getPokemonDishes")

const getDishes = (req, res, next) => {
    
    database.connect((err, client) => {
        getPokemonDishes(client, {}, list => res.json(list))
    })
}

module.exports = getDishes