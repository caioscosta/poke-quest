const database = require('../config/database');
const getSkillsList = require("../api/getSkillsList")

const getSkills = (req, res, next) => {
    
    database.connect((err, client) => {
        
        getSkillsList(client, list => res.json(list))
    })
}

module.exports = getSkills